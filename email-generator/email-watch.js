/* email-watch.js 

Watch for changes in email contents or templates and regenerate appropriate emails accordingly.

*/
const fs = require('fs')
const exec = require('child_process').exec
const pretty = require('pretty')

let templateFolder = 'fusionbible'
let testEmail = 'order-confirmation.txt'

let regenerate = function(emailString, callback) {
	dir = exec('node email-gen.js ' + emailString, function(err, stdout, stderr) {
		if (err) {
			console.log('error in execution:' + err)
		}

		console.log(stdout)
	})

	dir.on('exit', function (code) {
		console.log(code)
        callback()
	})
}

fs.watch('./templates/' + templateFolder + '/emails/', function (event, filename) {
    console.log(filename + ' ' + event + 'd')
    if (filename && filename.includes('.txt')) {
        console.log(filename + ' ' + event + 'd.')
        
        let emailString = fs.readFileSync('./templates/' + templateFolder + '/emails/' + filename, 'utf-8')
        regenerate(emailString, function() {

            let email = fs.readFileSync('email.html', 'utf8')
            let emailPretty = pretty(email)
            fs.writeFileSync('email-pretty.html', emailPretty, 'utf8')
            fs.writeFileSync('./templates/' + templateFolder + '/emails/' + filename.replace('.txt', '.html'), email, 'utf8')
        })
    } else {
        console.log('filename not provided')
    }
})

fs.watch('./templates/'+ templateFolder +'/', function (event, filename) {
    if (filename) {
        console.log(filename + ' ' + event + 'd.')
        
        if(filename.includes('.css')) {
            fs.readdirSync('./templates/' + templateFolder + '/emails').forEach(function(file) {
                
                if(file.includes('.txt')) {
                    
                    let emailString = fs.readFileSync('./templates/' + templateFolder + '/emails/' + file, 'utf8')
                    
                    regenerate(emailString, function() {

                        let email = fs.readFileSync('email.html', 'utf-8')
                        let emailPretty = pretty(email)
                        fs.writeFileSync('email-pretty.html', emailPretty, 'utf8')
                        fs.writeFileSync('./templates/' + templateFolder + '/emails/' + file.replace('.txt', '.html'), email, 'utf8')
                    })
                }
            });
        }
    } else {
        console.log('filename not provided')
    }
})