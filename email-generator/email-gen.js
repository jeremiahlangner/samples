const fs = require('fs')
const css = require('css')
const inlineCss = require('inline-css')
const minify = require('html-minifier').minify
const sizeOf = require('image-size')

let outputFile = 'email.html'
let platform = ''
let components = []
let templateCSS = {}
let templateFolder = ''
let emailString = ''
let fonts = ''

process.argv.forEach(function (val, index, array) {
	if(index >= 2) {
		if(val.includes(' ')) {
			val = '"' + val + '"'
		}

		emailString += val + ' '
	}		
})

console.log(emailString)

let isnum = function(val) {
	return /^\d+$/.test(val)
}

let checkCenter = function(htmlClasses) {
	let center = {}

	if(htmlClasses.includes('center')) {
		center.open = '<center>'
		center.close = '</center>'
	} else {
		center.open = ''
		center.close = ''
	}

	return center
}

let clearClasses = function(componentParts) {
	let content = ''

	for(let i in componentParts) {
		if(componentParts[i] != 'class') {
			content += ' ' + componentParts[i]
		}

		if(componentParts[i] == 'class') {
			let j = parseInt(i) + 1

			componentParts[i] = ''
			componentParts[j] = ''
		}
	}
	return content
}

let parseClasses = function(componentParts) {
	let htmlClasses = ''

	for(let i in componentParts) {
		if(componentParts[i] == 'class') {
			let j = parseInt(i) + 1; 
			htmlClasses += componentParts[j] + ' '
		}
	}

	return htmlClasses
}

let getContent = function(componentParts) {
	let html = ''

	for(let i = 0; i < componentParts.length; i++) {
		if(componentParts[i].includes('"')) {
			let innerComponent = ''
			componentParts[i] = componentParts[i].replace('"', '')

			inner:
			for(let j=i; j < componentParts.length; j++) {
				if(componentParts[j].includes('"')) {
					componentParts[j] = componentParts[j].replace('"', '')
					innerComponent += componentParts[j]
					componentParts[j] = ''
					i=j
					html += generateComponent(innerComponent)
					break inner
				} else {
					innerComponent += componentParts[j] + ' '
					componentParts[j] = ''
				}
			}
		}
	}
	return html
}

let generateComponent = function(component) {
	let componentParts = component.split(' ')
	let htmlTerms = ''
	let html = ''
	let content = ''
	let htmlClasses = ''
	let background = ''
	let center = {
		open: '',
		close: ''
	}

	if(componentParts[0] == 'emailPlatform') {

		platform = componentParts[1]

		componentParts.shift()
		componentParts.shift()

	} else if(componentParts[0] == 'template') {
		let fontFileName = ''
		componentParts[0] = ''

		templateFolder = './templates/' + componentParts[1].replace('.css', '/')

		if(componentParts[1].includes('.css')) {
			fontsFileName = templateFolder + componentParts[1].replace('.css', '') + '-fonts.html'
			if(fs.existsSync('templates/' + componentParts[1].replace('.css', '/') + componentParts[1])) {
				template = fs.readFileSync('templates/' + componentParts[1].replace('.css', '/') + componentParts[1], 'utf-8')
				templateCSS = css.parse(template)
			}
			componentParts[1] = ''
		} 
		
		if(fs.existsSync(fontsFileName)) {
			fonts = fs.readFileSync(fontsFileName, 'utf-8')
		}

		html += '<style>' + template + '</style>' 

	} else if(componentParts[0] == 'row') {
		componentParts[0] = ''
		content = getContent(componentParts)
		htmlClasses = parseClasses(componentParts).trim()
		center = checkCenter(htmlClasses)

		/* Get background color from Template CSS */
		let bgcolor = ''
		let classes = htmlClasses.split(' ')
		for(let i of templateCSS.stylesheet.rules) {
			for(let j of classes) {
				if(typeof i.selectors !== 'undefined' && typeof j !== 'undefined') {

					if(i.selectors.includes('.' + j)) {
						for(let q of i.declarations) {
							if(q.property == 'background-color') {
								bgcolor = q.value
							}
						}
					}

				}
			}
		}

		let nextComponentIndex = components.indexOf(component) + 1
		if(typeof components[nextComponentIndex] !== 'undefined') {
			let nextComponentParts = components[nextComponentIndex].split(' ')
			if(nextComponentParts[0] == 'columns') {
				row = '<table width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><td bgcolor="' + bgcolor + '" class="row ' + htmlClasses + '">' + center.open + content
			} else {
				row = '<table width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><td bgcolor="' + bgcolor + '" class="row ' + htmlClasses + '">' + center.open + content + center.close + '</td></tr></tbody></table>'
			}
		} else {
			row = '<table width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><td bgcolor="' + bgcolor + '" class="row ' + htmlClasses + '">' + center.open + content + center.close + '</td></tr></tbody></table>'
		}

		html += row

	} else if(componentParts[0] == 'columns') {
		componentParts[0] = ''
		let innerHTML = ''

		let previousComponentIndex = components.indexOf(component) - 1
		let previousComponentParts = (typeof components[previousComponentIndex] !== 'undefined') ? components[previousComponentIndex].split(' ') : []
		let rowEnding = (previousComponentParts[0] == 'row') ? '</td></tr></table>' : ''

		/* Iterate over columns */
		if(platform != 'BigCommerce') {
			for(let i=0; i < componentParts.length; i++) {
				if(isnum(componentParts[i])) {

					innerHTML += '<td width="' + componentParts[i] + '%" valign="top" border="0" style="margin-bottom:0;"><![endif]--><table class="ol col' + componentParts[i] + ' reorder" width="100%" align="left" cellpadding="0" cellspacing="0" border="0"><tbody><tr><td width="100%" valign="top">'

					let innerColumn = []
					innerColumn.push(componentParts[i])
					componentParts[i] = ''

					inner:
					for(let j=i; j < componentParts.length; j++) {
						if(isnum(componentParts[j]) || j == componentParts.length - 1) {
							i=j-1
							innerHTML += getContent(innerColumn) + '</td></tr></tbody></table><!--[if gte mso 9]></td>'
							break inner
						} else {
							innerColumn.push(componentParts[j])
						}
					}
				}
			}

			/* Add closing tags and conditionals */
			innerHTML += '</tr></table><![endif]--></td></tr></tbody></table>'

			// htmlClasses = parseClasses(componentParts).trim()

			html += '<table class="columns" width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><td class="' + htmlClasses + '"><!--[if gte mso 9]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>' + innerHTML + rowEnding
		} else {
			for(let i=0; i < componentParts.length; i++) {
				if(isnum(componentParts[i])) {
					innerHTML += '<td width="' + componentParts[i] + '%" valign="top" border="0"><![endif]--><td width="' + componentParts[i] + '%" align="left" cellpadding="0" cellspacing="0" border="0" valign="top" style="width:' + componentParts[i] + '%">'

					let innerColumn = []
					innerColumn.push(componentParts[i])
					componentParts[i] = ''

					inner:
					for(let j=i; j < componentParts.length; j++) {
						if(isnum(componentParts[j]) || j == componentParts.length - 1) {
							i=j-1
							innerHTML += getContent(innerColumn) + '</td><!--[if gte mso 9]></td>'
							break inner
						} else {
							innerColumn.push(componentParts[j])
						}
					}
				}
			}
			/* Add closing tags and conditionals */
			innerHTML += '</tr></table><![endif]--></tr></tbody></table>'

			

			// htmlClasses = parseClasses(componentParts).trim()
			html += '<table class="columns" width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><!--[if gte mso 9]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>' + innerHTML + rowEnding
		}
	} else if(componentParts[0].match(/^p|h1|h2|h3|h4|h5|h6$/)) {
		let tag = componentParts[0]

		componentParts[0] = ''

		htmlClasses = parseClasses(componentParts).trim()
		center = checkCenter(htmlClasses)
		content = clearClasses(componentParts).trim()

		html += '<table width="100%" border="0" cell-padding="0" cell-spacing="0"><tbody><tr><td class="' + htmlClasses +'">' + center.open + '<' + tag + '>' + content + '</' + tag + '>' + center.close + '</td></tr></tbody></table>'

	} else if(componentParts[0] == 'button') {
		componentParts[0] = ''
		htmlClasses = parseClasses(componentParts).trim()
		let link = ''

		for(let i = 0; i < componentParts.length; i++) {
			if(componentParts[i].includes('href')) {
				componentParts[i] = ''
				link = componentParts[i+1]
				componentParts[i] = ''
				componentParts[i+1] = ''
			}
		}

		content = clearClasses(componentParts).trim()

		html += '<table width="" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" class="' + htmlClasses + '"><center><p><a href="' + link + '">' + content + '</a></p></center></td></tr></table>'

	} else if(componentParts[0] == 'img') {
		componentParts[0] = ''
		let htmlClasses = parseClasses(componentParts).trim()
		let content = clearClasses(componentParts).trim()
		let imageSize = {
			width: '100%',
			height: '100%'
		}

		if(fs.existsSync(templateFolder + '/emails/' + content)) {
			imageSize = sizeOf(templateFolder + '/emails/' + content)
		}

		center = checkCenter(htmlClasses)

		html += '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td width="100%" valign="top" class="' + htmlClasses + '" style="max-width:' + imageSize.width + 'px">' + center.open + '<img width="100%" src="' + content + '" style="max-width:' + imageSize.width +'px" />' + center.close + '</td></tr></tbody></table>'

	} else {
		let htmlClasses = parseClasses(componentParts).trim()
		let content = clearClasses(componentParts).trim()

		html += '<p' + ' class="' + htmlClasses + '">' + content + '</p>'
	}

	return html
}

let generateEmail = function(emailString) {
	/* Assign objects */
	components = emailString.split('-')
	let html = ''
	
	for(let i in components) {
		html += generateComponent(components[i])
	}

	let bodyBackground = ''
	if(typeof templateCSS !== 'undefined') {
		for(let i of templateCSS.stylesheet.rules) {
			if(typeof i.selectors !== 'undefined')
			if(i.selectors.includes('.bodyBackground')) {
				for(let j of i.declarations) {
					if(j.property = 'background-color') {
						bodyBackground = j.value
					}
				}
			}		
		}
	}

	let finalHTML = '<body background="" bgcolor="" style="background-image: url(); background-position: center top;"> \n' +
    '<table class="bodyContainer" width="100%" bg-color="' + bodyBackground + '" cellpadding="0" cellspacing="0" style="min-width:100%; background-position: center top; background-repeat: repeat;"> \n' + 
        '<tbody><tr><td><center>'

    /* Container: Keeping separate for the time being so I may replace later. */
    finalHTML += '<!--[if gte mso 9]><table width="640" cellpadding="0" cellspacing="0"><tr><td><![endif]--> \n' +
            '<table class="container640" width="100%" style="max-width:640px; margin: 0 auto;" cellpadding="0" cellspacing="0" border="0"><tbody><tr><td width="100%">' + html

	/* Append inline CSS from template */
	inlineCss(finalHTML, { url: '.'})
	.then(function(output) {
		let responsiveStyles = fs.readFileSync('./responsive.html')

		finalHTML = '<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title></title>' + fonts + responsiveStyles + output

		finalHTML += '</td></tr></tbody></table><!--[if gte mso 9]></td></tr></table><![endif]-->  </center></td></tr></tbody></table></body></html>'

		finalHTML = minify(finalHTML, { minifyCSS:true })
		fs.writeFileSync(outputFile, finalHTML)
	})
}

generateEmail(emailString)