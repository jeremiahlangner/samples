class typeWriter {
	constructor(data, selector) {
		this.data = data
		this.selector = selector
		this.wait(5, 0)
	}

	calcOrganic() {
		return 38 + (100 * Math.random())
	}

	getTextIndex(data) {
		for(let i = this.data.length - 1; i >= 0; i--) {
			if(data.text == this.data[i].text) {
				return i
			}
		}
	}

	backSpace(data, i) {
		let scope = this;
		let strokeTime = this.calcOrganic()

		if(i >= 0) {
			document.querySelector(this.selector).innerHTML = data.text.substring(0, i) + '<span class="cursor">|</span>'

			setTimeout(() => {
                scope.backSpace(data, i - 1)
            }, strokeTime)
		} else {
			i = this.getTextIndex(data)

			if(typeof(this.data[i+1]) !== 'undefined') {
				setTimeout(() => {
					scope.type(scope.data[i+1], 0)
				}, 500)
			} else {
				setTimeout(() => {
					scope.type(scope.data[0], 0)
				}, 500)
			}
		}
	}

	wait(interval, i) {
		let scope = this
		document.querySelector(this.selector).classList.add('emphasis')
		setTimeout(() => {
			document.querySelector(scope.selector).classList.remove('emphasis')
		}, 1500)

		if(i < this.data.length) {
			setTimeout(() => {
				scope.backSpace(scope.data[i], scope.data[i].text.length)
			}, interval)
		}
	}

	type(data, i) {
		let scope = this;
		let strokeTime = this.calcOrganic()

        if(i < (data.text.length)) {
            if(data.text[i] == '<') {
                let j = 0;
                let k = 0;
                for(let x = 0; x<data.text.substring(i, data.text.length).length; x++) {
                    if(data.text[i+x] === ' ') {
                        k = x
                    }

                    if(data.text[i+x] == '/') {
                        k = 'forty-two'
                    }

                    if(data.text[i+x] == '>') {
                        j = x
                        if(k == 0){
                            k = x
                        }
                        break
                    }
                }

                if(k != 'forty-two') { 
                    document.querySelector(this.selector).innerHTML = data.text.substring(0, i) + data.text.substring(i, i+j+1) + '</' + data.text.substring(i+1, i + k) + '>' + '<span class="cursor">|</span>'
                } else {
                    document.querySelector(this.selector).innerHTML = data.text.substring(0, i) + data.text.substring(i, i+j+1) + '<span class="cursor">|</span>'
                }

                setTimeout(() => {
                    scope.type(data, i + j + 1)
                }, strokeTime)
            } else {
                document.querySelector(this.selector).innerHTML = data.text.substring(0, i+1) + '<span class="cursor">|</span>';
                setTimeout(() => {
                    scope.type(data, i + 1)
                }, strokeTime)
            } 
        } else {
        	i = this.getTextIndex(data)

        	if(typeof(this.data[i+1]) !== 'undefined') {
            	this.wait(3500, i)
            } else {
            	setTimeout(() => {
            		// document.querySelector('.cursor').style.display = 'none';
            		document.querySelector(scope.selector).classList.add('emphasis')
            		setTimeout(() => {
            			document.querySelector(scope.selector).classList.remove('emphasis')
            		}, 1500)
            	}, 3000)
        		this.wait(5000, i)
            }
        }
    }
}

const textData = [
	{
		"text":'of your dreams'
	},
	{
		"text":'you need'
	},
	{
		"text":'your little ones will love'
	}
]

const typewriter = new typeWriter(textData, '.yellow')	

class svgMapObject {
	constructor(locations) {
		this.locations = locations
		this.positionFlags()
		this.handleEvents()
	}

	handleEvents() {
		for(let location of this.locations) {
			const el = document.getElementById(location.id)
			
			el.addEventListener('click', (event) => {
				window.location.href = location.url
			}, false) 
		}

		window.addEventListener('resize', this.resizeContainer);
	}

	positionFlags() {
		for(let location of this.locations) {
			const el = document.getElementById(location.id)

			el.style.transformOrigin = '0 0'
			el.style.transform = `scale(${location.scale})`
			el.style.left = `${location.position.x}%`
			el.style.marginTop = `${location.position.y}%`
		}
	}

	resizeContainer() {
		document.getElementById('mapContainer').style.height = document.getElementById('sittersMap').offsetHeight + 'px';
	}
}

function positionSocialIcons() {
	if(document.querySelector('.hero-slider-column') != null) {
		let socialIcons = document.getElementById('social-icons')
		let hero = document.querySelector('.hero-slider-column')

		hero.appendChild(socialIcons)
		socialIcons.style.position = 'absolute'
		socialIcons.style.top = '-130px'
		socialIcons.style.right = '24px'
	} else {
		let socialIcons = document.getElementById('social-icons')
		let hero = document.querySelector('.et_pb_section_0')

		hero.appendChild(socialIcons)
		socialIcons.style.position = 'absolute'
		socialIcons.style.top = (hero.getBoundingClientRect().bottom - 60) +'px';
		socialIcons.style.right = '24px'
	}
}

if(document.querySelector('.mapContainer') != null) {
	const locations = [
		{
			"id": "CharlotteNC",
			"url": "./charlotte-nc/",
			"position": {
				"x":"41.2", 
				"y":"4.9"
			},
			"scale": "0.215"
		},
		{
			"id": "UpstateSC",
			"url": "./greenville-sc/",
			"position": {
				"x":"22.75",
				"y":"15.75"
			},
			"scale": "0.187"
		},
		{
			"id": "CharlestonSC",
			"url": "./charleston-sc/",
			"position": {
				"x":"59.75", 
				"y":"43.9"
			},
			"scale": "0.23"
		}
	]

	const map = new svgMapObject(locations)
}

positionSocialIcons()
window.addEventListener('resize', positionSocialIcons)
