<?php
/**
 * 
 * Plugin Name: Smart Sitters 
 * Plugin URI:
 * Description: Adds SVG map navigation and animations to SmartSitters sites.
 * Version: 1.0.0
 * Author: Jeremiah Langner
 * Author URI: https://jeremiahlangner.me
 * 
 */

$PLUGIN_PAGE_NAME = 'smartsitters';

/* Prevent external calls */
if ( ! defined( 'WPINC' )) {
    die;
}

/* Prevent accidental auto updates. */
function dm_prevent_update_check( $r, $url ) {
    if ( 0 === strpos( $url, 'http://api.wordpress.org/plugins/update-check/' ) ) {
        $plugin = plugin_basename( __FILE__ );
        $plugins = unserialize( $r['body']['plugins'] );
        unset( $plugins->plugins[$plugin] );
        unset( $plugins->active[array_search( $my_plugin, $plugins->active )] );
        $r['body']['plugins'] = serialize( $plugins );
    }
    return $r;
}
add_filter( 'http_request_args', 'dm_prevent_update_check', 10, 2 );

/* Enqueue styles. */
function enqueue_sitters_plugin_styles() {
    wp_enqueue_style( 'sitters-plugin-styles', plugin_dir_url( __FILE__ ) . 'css/styles.css', array() );
}
add_action( 'get_header', 'enqueue_sitters_plugin_styles' );

/* Enqueue scripts. */
function enqueue_sitters_plugin_scripts() {
    wp_enqueue_script( 'sitters-plugin-scripts', plugin_dir_url( __FILE__ ) . 'js/plugin.bundle.min.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_sitters_plugin_scripts' );

/* Add shortcode. */
function plugin_shortcode() {
    $html = '<div id="mapContainer"><img id="sittersMap" src="' . plugin_dir_url( __FILE__ ) . 'images/smart_sitters_map.svg" /><img id="CharlestonSC" class="locationFlag" src="' . plugin_dir_url( __FILE__ ) . 'images/charleston_flag.svg" /><img id="UpstateSC" class="locationFlag" src="' . plugin_dir_url( __FILE__ ) . 'images/upstate_flag.svg" /><img id="CharlotteNC" class="locationFlag" src="' . plugin_dir_url( __FILE__ ) . 'images/charlotte_flag.svg" /></div>';

    return $html;
}
add_shortcode( 'sitters-plugin-shortcode', 'plugin_shortcode' );
