<?php
/**
 * Template Name: Case Study
 * Based on Avada full-width template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>

<?php get_header(); ?>

<!-- Close default theme header tags. -->
	</div>
</main>

<?php echo do_shortcode('[fusion_fusionslider name="case-study-slider" hide_on_mobile="small-visibility,medium-visibility,large-visibility" class="" id=""][/fusion_fusionslider]'); ?>

<main id="main" role="main" class="clearfix " style="">
	<div class="fusion-row">
		<div class="case-study-width">
			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php echo fusion_render_rich_snippets_for_pages(); // WPCS: XSS ok. ?>
					<?php avada_featured_images_for_pages(); ?>
					<div class="post-content-helper">
						<?php the_content(); ?>
						<?php fusion_link_pages(); ?>
					</div>
					<?php if ( ! post_password_required( $post->ID ) ) : ?>
						<?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
							<?php wp_reset_postdata(); ?>
							<?php comments_template(); ?>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				
				<?php
					$testimonial = get_field('testimonial');
					$attribution = get_field('name'); 
				?>
				
				<!-- Assign title based on post title. -->
				<script>
					let title = '<?php the_title(); ?>';
					document.querySelector('.fusion-title h2').innerHTML = title;

					window.onload = function() {
						let imageLink = document.querySelector('.flex-active-slide a');
						imageLink.style.cursor = 'default';
						imageLink.href = '#';
					};
				</script>
			<?php endwhile; ?>
		</div>
	</div>
</main>

<div class="testimonial-wrapper">
	<main id="main" role="main" class="clearfix " style="background-color: transparent;">
		<div class="fusion-row">
			<div class="testimonial-left-quote">&ldquo;</div>
			<div class="testimonial-container">
				<div class="testimonial-quote">
					<span class="testimonial">
						<?php echo $testimonial ?>
					</span>
				</div>
				<div class="testimonial-attribution">
					<span class="name">
						&mdash; <?php echo $attribution ?>
					</span>
				</div>
			</div>
			<div class="testimonial-right-quote">&rdquo;</div>
		</div>
	</main>
</div>

<!-- Reopen default classes -->
<main role="main" class="clearfix " style="">
	<div class="fusion-row">
<?php
get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */