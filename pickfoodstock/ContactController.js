/**
 * ContactController
 *
 * @description :: Server-side logic for contact form
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var mandrill = require('mandrill-api/mandrill'),
	mandrillClient = new mandrill.Mandrill('...API key scraped...'),
	validator = require('validator');

exports.contactForm = function (req, res) {
	res.view('public/general/contact', {
		title: 'Contact Us'
	});
};

exports.postForm = function (req, res, next) {
	if ((!validator.isEmpty(req.param('email')) && validator.isEmail(req.param('email'))) && !validator.isEmpty(req.param('message'))) {
		var message = {
			text: req.param('message'),
			subject: 'Inquiry from ' + req.param('email'),
			from_email: '...',
			from_name: req.param('email'),
			to: [{
				email: '...',
				name: 'PFS Admin',
				type: 'to'
			}],
			headers: {
				'Reply-To': req.param('email')
			},
			important: false,
			track_opens: null,
			track_clicks: null,
			auto_text: null,
			auto_html: null,
			inline_css: null,
			url_strip_qs: null,
			preserve_recipients: null,
			view_content_link: null,
			tracking_domain: null,
			signing_domain: null,
			return_path_domain: null,
			merge: true,
			merge_language: 'mailchimp',
		};

		var async = false,
			ipPool = 'Main Pool',
			sendAt = 'example send_at';

		mandrillClient.messages.send({
				message: message,
				async: async,
				ip_pool: ipPool /*, send_at: sendAt */
			},

			function (result) {
				if (result[0].status !== 'sent') {
					req.session.flash = {
						err: {
							name: 'required',
							message: 'There was an error with our email sending service. Please reach out via one of our social networks if this problem persists!',
							code: result
						}
					};

					res.redirect('/contact');
				} else {
					req.session.flash = {
						success: {
							name: 'success',
							message: 'Your message has been received! We will be in touch with you soon!'
						}
					};

					res.redirect('/contact');
				}


			}, function (e) {
				req.session.flash = {
					err: {
						name: 'required',
						message: 'There was an error with our email sending service. Please reach out via one of our social networks if this problem persists!',
						code: e
					}
				};

				res.redirect('/contact');
			});
	} else {
		req.session.flash = {
			err: {
				name: 'required',
				message: 'Please enter an email address and message'
			}
		};

		res.redirect('/contact');
	}
};