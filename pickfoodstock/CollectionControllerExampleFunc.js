exports.admin_list = function (req, res, next) {
	const limit = 50;

	let skip = 0, // number of items to skip
		page = parseInt(req.param('page') || req.query.page, 10);

	if (_.isFinite(page)) {
		skip = (page - 1) * limit;
	} else {
		page = 1;
	}

	Collection.find().then(function (collections) {
		return Photo.find({
			photo_id: { $in: _.pluck(collections, 'photo') }
		}).then(function (photos) {
			return sails.controllers.draft.getSignedUrls(photos, { thumb: 900 });
		}).then(function (photos) {
			collections = _.map(collections, function (collection) {
				collection.image_url_thumb = (_.findWhere(photos, { photo_id: collection.photo }) || {}).image_url_thumb;

				return collection;
			});
		}).then(function () {
			return Collection.count().then(function (count) {
				Photo.native(function (err, collection) {
					collection.aggregate([

						// mongodb 3.2 method
						{ $unwind: '$collections' },
						{
							$group: {
								_id: '$collections',
								total_photos: {
									$sum: 1
								}
							}
						}
					]).toArray(function (err, results) {
						if (err) {
							next(err);
						} else {
							collections.forEach(function (coll, index) {
								const match = _.find(results, { _id: coll.collection_id });
								if (collections[index]) {
									collections[index].total_photos = (match && match.total_photos) ? match.total_photos : 0;
								}
							});

							res.view('admin/collections/collections', {
								collections: collections,
								limit: limit,
								skip: skip,
								page: page,
								count: count
							});
						}
					});
				});
			});
		});
	}).catch(function (err) {
		next(err);
	});
};

exports.admin_view = function (req, res, next) {
	const limit = 50;

	let skip = 0, // number of items to skip
		page = parseInt(req.param('page') || req.query.page, 10);

	if (_.isFinite(page)) {
		skip = (page - 1) * limit;
	} else {
		page = 1;
	}

	if (req.param('id')) {
		Collection.findOne({
			url: req.param('id')
		}).then(function (collection) {
			if (!collection) {
				next();
			} else {
				return Photo.find({
					photo_id: collection.photo
				}).then(function (photos) {
					return sails.controllers.draft.getSignedUrls(photos, { thumb: 900 });
				}).then(function (photos) {
					collection.image_url_thumb = (_.findWhere(photos, { photo_id: collection.photo }) || {}).image_url_thumb;
				}).then(function () {
					return Photo.count({
						collections: collection.collection_id
					});
				}).then(function (count) {
					return Photo.find({
						collections: collection.collection_id,
						limit: limit,
						skip: skip
					}).then(function (photos) {
						return sails.controllers.draft.getSignedUrls(photos, { thumb: 900 });
					}).then(function (photos) {
						res.view('admin/collections/collection', {
							title: collection.name,
							collection: collection,
							photos: photos,
							limit: limit,
							page: page,
							count: count
						});
					});
				});
			}
		}).catch(function (err) {
			next(err);
		});
	}
};

